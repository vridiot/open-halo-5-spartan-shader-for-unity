This should play nicely with all Halo 5 MP Spartan Textures etc, and is written using the amplify shader editor and is clearly commented should anyone wish to edit it.

This shader was Made with the Amplify Shader Editor for users to make changes as they see fit, while giving users a quick way to get a custom spartan Coloured.

It is Designed to work with the Halo 5 Colour, Control and Normal Map Files for the Muliplayer spartans specifically.

Please note that this shader was made under the GNU LESSER GENERAL PUBLIC LICENSE  Version 2.1, February 1999
